package cargoShipment;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import cargoShipment.CheckConstraints;


public class ShipmentDate {
	int idealHrs;
	String startDate;

	public static void main(String[] args){
		ShipmentDate sd = new ShipmentDate("31-12-1999 15:00:00", 17);
        	System.out.println("Shipment arrives on " + sd.getDate());
	}
	
	
	public String getDate(){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(this.startDate, formatter);
        
        int idealHrs = this.idealHrs;
        int numberOfHrsPerDay = PerDayConstraints.HOURS.numberOfHrsPerDay;

        CheckConstraints check = new CheckConstraints();
        
        while( true ){
        	if(!check.isHoliday(dateTime)){
        		int remainingHrs = this.remainingHrsInTheDay(dateTime);
        		int hrsToAdd = Math.min(remainingHrs, numberOfHrsPerDay);
        		hrsToAdd = Math.min(hrsToAdd, idealHrs);
        		dateTime = dateTime.plusHours(hrsToAdd);
        		idealHrs -= hrsToAdd;
        	}
        	if(idealHrs > 0)
        		dateTime = this.nextDayMidnight(dateTime);
        	else
        		break;
        }
        String currTime = dateTime.toLocalTime().toString();
        String day = dateTime.getDayOfWeek().name().toLowerCase();
        return day + " " + currTime;
	}

	
	
	public ShipmentDate(String startDate, int idealHrs){
		this.idealHrs = idealHrs;
		this.startDate = startDate;
	}
	
	
	
	public int remainingHrsInTheDay(LocalDateTime dateTime){
		int hours = (int)dateTime.until( nextDayMidnight(dateTime), ChronoUnit.HOURS );
		return hours;
	}

	
	
	public LocalDateTime nextDayMidnight(LocalDateTime dateTime){
		LocalTime midnight = LocalTime.MIDNIGHT;
		LocalDate date = dateTime.toLocalDate().plusDays(1);
		return LocalDateTime.of(date, midnight);
	}
}